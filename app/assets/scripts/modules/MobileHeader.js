import $ from 'jquery';

class MobileHeader{
    constructor(){
        this.header = $(".header");
        this.menuIcon = $(".header__menu-icon");
        this.menuContainer = $(".header__menu-container");
        this.events();
    }
    
    events(){
        this.menuIcon.click(this.toggleMobileHeader.bind(this));
    }
    
//    In the bind method by passing this we can change the "this" of the method which is called which is bydeafullty the invoking object
//    So for events it is MobileHeaeder as "this" so by passing that we get Mobile Haeader in the toggleMobileHeader which was menuIcon if not
    toggleMobileHeader(){
        this.menuContainer.toggleClass("header__menu-container__is-visible");
        this.menuIcon.toggleClass("header__menu-icon__close");
        this.header.toggleClass("header__is-expanded");
        
//        console.log(this);
    }
}
export default MobileHeader;