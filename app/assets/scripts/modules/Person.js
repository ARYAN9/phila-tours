//This is es-5 format (no class)
class Person{
    constructor(fullName,favColor){
        this.name = fullName;
        this.favColor = favColor;
    }
    greet(){
        console.log("Hello Welcome! My name is " + this.name + " and I love " + this.favColor + " color!");        
    }
}
//function Person(fullName,favColor){
//    this.name = fullName;
//    this.favColor = favColor;
//    
//    this.greet = function(){
//        console.log("My name is " + this.name + " and I love " + this.favColor + " color!");
//    }
//}
//module.exports = Person;

//Joo export karenge voo hii retturn hogaa!!!
export default Person;